################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../linker.cmd 

SYSCFG_SRCS += \
../example.syscfg 

C_SRCS += \
../board.c \
./syscfg/ti_dpl_config.c \
./syscfg/ti_drivers_config.c \
./syscfg/ti_drivers_open_close.c \
./syscfg/ti_pinmux_config.c \
./syscfg/ti_power_clock_config.c \
./syscfg/ti_board_config.c \
./syscfg/ti_board_open_close.c \
./syscfg/ti_enet_config.c \
./syscfg/ti_enet_open_close.c \
./syscfg/ti_enet_soc.c \
../i2c_led_blink.c \
../main.c 

GEN_FILES += \
./syscfg/ti_dpl_config.c \
./syscfg/ti_drivers_config.c \
./syscfg/ti_drivers_open_close.c \
./syscfg/ti_pinmux_config.c \
./syscfg/ti_power_clock_config.c \
./syscfg/ti_board_config.c \
./syscfg/ti_board_open_close.c \
./syscfg/ti_enet_config.c \
./syscfg/ti_enet_open_close.c \
./syscfg/ti_enet_soc.c 

GEN_MISC_DIRS += \
./syscfg/ 

C_DEPS += \
./board.d \
./syscfg/ti_dpl_config.d \
./syscfg/ti_drivers_config.d \
./syscfg/ti_drivers_open_close.d \
./syscfg/ti_pinmux_config.d \
./syscfg/ti_power_clock_config.d \
./syscfg/ti_board_config.d \
./syscfg/ti_board_open_close.d \
./syscfg/ti_enet_config.d \
./syscfg/ti_enet_open_close.d \
./syscfg/ti_enet_soc.d \
./i2c_led_blink.d \
./main.d 

OBJS += \
./board.o \
./syscfg/ti_dpl_config.o \
./syscfg/ti_drivers_config.o \
./syscfg/ti_drivers_open_close.o \
./syscfg/ti_pinmux_config.o \
./syscfg/ti_power_clock_config.o \
./syscfg/ti_board_config.o \
./syscfg/ti_board_open_close.o \
./syscfg/ti_enet_config.o \
./syscfg/ti_enet_open_close.o \
./syscfg/ti_enet_soc.o \
./i2c_led_blink.o \
./main.o 

GEN_MISC_FILES += \
./syscfg/ti_dpl_config.h \
./syscfg/ti_drivers_config.h \
./syscfg/ti_drivers_open_close.h \
./syscfg/ti_board_config.h \
./syscfg/ti_board_open_close.h \
./syscfg/ti_enet_config.h \
./syscfg/ti_enet_open_close.h \
./syscfg/ti_pru_io_config.inc 

GEN_MISC_DIRS__QUOTED += \
"syscfg/" 

OBJS__QUOTED += \
"board.o" \
"syscfg/ti_dpl_config.o" \
"syscfg/ti_drivers_config.o" \
"syscfg/ti_drivers_open_close.o" \
"syscfg/ti_pinmux_config.o" \
"syscfg/ti_power_clock_config.o" \
"syscfg/ti_board_config.o" \
"syscfg/ti_board_open_close.o" \
"syscfg/ti_enet_config.o" \
"syscfg/ti_enet_open_close.o" \
"syscfg/ti_enet_soc.o" \
"i2c_led_blink.o" \
"main.o" 

GEN_MISC_FILES__QUOTED += \
"syscfg/ti_dpl_config.h" \
"syscfg/ti_drivers_config.h" \
"syscfg/ti_drivers_open_close.h" \
"syscfg/ti_board_config.h" \
"syscfg/ti_board_open_close.h" \
"syscfg/ti_enet_config.h" \
"syscfg/ti_enet_open_close.h" \
"syscfg/ti_pru_io_config.inc" 

C_DEPS__QUOTED += \
"board.d" \
"syscfg/ti_dpl_config.d" \
"syscfg/ti_drivers_config.d" \
"syscfg/ti_drivers_open_close.d" \
"syscfg/ti_pinmux_config.d" \
"syscfg/ti_power_clock_config.d" \
"syscfg/ti_board_config.d" \
"syscfg/ti_board_open_close.d" \
"syscfg/ti_enet_config.d" \
"syscfg/ti_enet_open_close.d" \
"syscfg/ti_enet_soc.d" \
"i2c_led_blink.d" \
"main.d" 

GEN_FILES__QUOTED += \
"syscfg/ti_dpl_config.c" \
"syscfg/ti_drivers_config.c" \
"syscfg/ti_drivers_open_close.c" \
"syscfg/ti_pinmux_config.c" \
"syscfg/ti_power_clock_config.c" \
"syscfg/ti_board_config.c" \
"syscfg/ti_board_open_close.c" \
"syscfg/ti_enet_config.c" \
"syscfg/ti_enet_open_close.c" \
"syscfg/ti_enet_soc.c" 

C_SRCS__QUOTED += \
"../board.c" \
"./syscfg/ti_dpl_config.c" \
"./syscfg/ti_drivers_config.c" \
"./syscfg/ti_drivers_open_close.c" \
"./syscfg/ti_pinmux_config.c" \
"./syscfg/ti_power_clock_config.c" \
"./syscfg/ti_board_config.c" \
"./syscfg/ti_board_open_close.c" \
"./syscfg/ti_enet_config.c" \
"./syscfg/ti_enet_open_close.c" \
"./syscfg/ti_enet_soc.c" \
"../i2c_led_blink.c" \
"../main.c" 

SYSCFG_SRCS__QUOTED += \
"../example.syscfg" 



################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: Arm Compiler'
	"/home/quentin/ti/ccs1210/ccs/tools/compiler/ti-cgt-armllvm_2.1.2.LTS/bin/tiarmclang" -c -mcpu=cortex-r5 -mfloat-abi=hard -mfpu=vfpv3-d16 -mlittle-endian -mthumb -I"/home/quentin/ti/ccs1210/ccs/tools/compiler/ti-cgt-armllvm_2.1.2.LTS/include/c" -I"/home/quentin/ti/mcu_plus_sdk_am64x_08_04_00_17/source" -I"/home/quentin/ti/mcu_plus_sdk_am64x_08_04_00_17/source/kernel/freertos/FreeRTOS-Kernel/include" -I"/home/quentin/ti/mcu_plus_sdk_am64x_08_04_00_17/source/kernel/freertos/portable/TI_ARM_CLANG/ARM_CR5F" -I"/home/quentin/ti/mcu_plus_sdk_am64x_08_04_00_17/source/kernel/freertos/config/am64x/r5f" -DSOC_AM64X -D_DEBUG_=1 -g -Wall -Wno-gnu-variable-sized-type-not-at-end -Wno-unused-function -MMD -MP -MF"$(basename $(<F)).d_raw" -MT"$(@)" -I"/home/quentin/workspace_v12/i2c_led_blink_am64x-sk_r5fss0-0_freertos_ti-arm-clang/Debug/syscfg"  $(GEN_OPTS__FLAG) -o"$@" "$(shell echo $<)"
	@echo 'Finished building: "$<"'
	@echo ' '

build-259960289: ../example.syscfg
	@echo 'Building file: "$<"'
	@echo 'Invoking: SysConfig'
	"/home/quentin/ti/ccs1210/ccs/utils/sysconfig_1.14.0/sysconfig_cli.sh" -s "/home/quentin/ti/mcu_plus_sdk_am64x_08_04_00_17/.metadata/product.json" --script "/home/quentin/workspace_v12/i2c_led_blink_am64x-sk_r5fss0-0_freertos_ti-arm-clang/example.syscfg" --context "r5fss0-0" -o "syscfg" --part Default --package ALV --compiler ticlang
	@echo 'Finished building: "$<"'
	@echo ' '

syscfg/ti_dpl_config.c: build-259960289 ../example.syscfg
syscfg/ti_dpl_config.h: build-259960289
syscfg/ti_drivers_config.c: build-259960289
syscfg/ti_drivers_config.h: build-259960289
syscfg/ti_drivers_open_close.c: build-259960289
syscfg/ti_drivers_open_close.h: build-259960289
syscfg/ti_pinmux_config.c: build-259960289
syscfg/ti_power_clock_config.c: build-259960289
syscfg/ti_board_config.c: build-259960289
syscfg/ti_board_config.h: build-259960289
syscfg/ti_board_open_close.c: build-259960289
syscfg/ti_board_open_close.h: build-259960289
syscfg/ti_enet_config.c: build-259960289
syscfg/ti_enet_config.h: build-259960289
syscfg/ti_enet_open_close.c: build-259960289
syscfg/ti_enet_open_close.h: build-259960289
syscfg/ti_enet_soc.c: build-259960289
syscfg/ti_pru_io_config.inc: build-259960289
syscfg/: build-259960289

syscfg/%.o: ./syscfg/%.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: Arm Compiler'
	"/home/quentin/ti/ccs1210/ccs/tools/compiler/ti-cgt-armllvm_2.1.2.LTS/bin/tiarmclang" -c -mcpu=cortex-r5 -mfloat-abi=hard -mfpu=vfpv3-d16 -mlittle-endian -mthumb -I"/home/quentin/ti/ccs1210/ccs/tools/compiler/ti-cgt-armllvm_2.1.2.LTS/include/c" -I"/home/quentin/ti/mcu_plus_sdk_am64x_08_04_00_17/source" -I"/home/quentin/ti/mcu_plus_sdk_am64x_08_04_00_17/source/kernel/freertos/FreeRTOS-Kernel/include" -I"/home/quentin/ti/mcu_plus_sdk_am64x_08_04_00_17/source/kernel/freertos/portable/TI_ARM_CLANG/ARM_CR5F" -I"/home/quentin/ti/mcu_plus_sdk_am64x_08_04_00_17/source/kernel/freertos/config/am64x/r5f" -DSOC_AM64X -D_DEBUG_=1 -g -Wall -Wno-gnu-variable-sized-type-not-at-end -Wno-unused-function -MMD -MP -MF"syscfg/$(basename $(<F)).d_raw" -MT"$(@)" -I"/home/quentin/workspace_v12/i2c_led_blink_am64x-sk_r5fss0-0_freertos_ti-arm-clang/Debug/syscfg"  $(GEN_OPTS__FLAG) -o"$@" "$(shell echo $<)"
	@echo 'Finished building: "$<"'
	@echo ' '


